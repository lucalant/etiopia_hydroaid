---
title: "WashUP User Manual"
abstract: The WashUP QGIS Project is a GIS tool created to collect, store and manage information about Water Supply Infrastructure (WSI). It allow to create an inventory with the geographical position of the infrastructure and collect administrative and technical information. It also possible store the water quality and intervention status and monitoring. The tool was developed into QGIS, an Open Source Software. This technical document represent the User Manual of the WashUP system. 
author: Luca Lanteri, Rocco Pispico.
date: Decembrer 16, 2023
geometry: margin=3cm
output: pdf_document
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[CO,CE]{}
    \fancyfoot[CO,CE]{\author}
    \fancyfoot[LE,RO]{\thepage}

---

\newpage

# WASH-UP Project - QGIS user guide

## Preamble

The WashUP QGIS Project is a GIS tool created to collect, store and manage information about Water Supply Infrastructure (WSI). It allow to create an inventory with the geographical position of the infrastructure and collect administrative and technical information. It also possible store the water quality and intervention status and monitoring. This user guide describes the main functionality of the software and how to use it. WashUP QGIS Project was developed from January 2022 to May 2022 and was improved after the suggestion of the users collected during follow up activity, carried out in Ethiopia on the summer 2023. 

The latest version was released in December 2023.

The last version is tested and relased with QGIS 3.28. 

This document represent only the user manual of the WashUP specific software, not for QGIS. 

The QGIS User Guide is available [here](https://docs.qgis.org/3.22/en/docs/user_manual/index.html "link")

**License of this document**

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation.

## Foreword

QGIS is an Open Source Geographic Information System. The project was born in May of 2002 and was established as a project on SourceForge in June of the same year. We've worked hard to make GIS software (which is traditionally expensive proprietary software) a viable prospect for anyone with basic access to a personal computer. QGIS currently runs on most Unix platforms, Windows, and macOS. QGIS is developed using the Qt toolkit ([https://www.qt.io](https://www.qt.io/)) and C++. This means that QGIS feels snappy and has a pleasing, easy-to-use graphical user interface (GUI).

QGIS aims to be a user-friendly GIS, providing common functions and features. The initial goal of the project was to provide a GIS data viewer. QGIS has reached the point in its evolution where it is being used by many for their daily GIS data-viewing needs. QGIS supports a number of raster and vector data formats, with new format support easily added using the plugin architecture.

QGIS is released under the GNU General Public License (GPL). Developing QGIS under this license means that you can inspect and modify the source code, and guarantees that you, our happy user, will always have access to a GIS program that is free of cost and can be freely modified. You should have received a full copy of the license with your copy of QGIS, and you also can find it in Appendix [*GNU General Public License*](https://docs.qgis.org/2.18/en/docs/user_manual/appendices/appendices.html#gpl-appendix).  



\newpage

# Getting Started

## QGIS installation

QGIS can be installed in many ways on different operative system, Please refer to the QGIS [User guide](https://docs.qgis.org/3.22/en/docs/user_manual/introduction/getting_started.html#installing-qgis)

## WASH-UP installation

To install software you've just to copy all the project files, generally delivered by a USB stick the last days of the courses into a folder on your PC. 

We have developed two different projects: *washUP_north* and *washUP_south*; these projects are the same in terms of functionality, the only difference is the basic cartographic data. This choice allow a greater efficiency in data management, because the the cartographic data represents the heavy data in the project.

## Data sources

- The WASH-UP QGIS project takes data from several sources:

  - **ha_data.gpkg**: the specific data of the project. Contains data of all water supply infrastructures, water supply sources and hydro schemes. Note: <u>Back up this file often</u> because it contains your working data.
  - **ha_dict.gpkg**: Contains all data dictionaries used in UI interaction, for example the list of regiones, woredas, kebeles or pump types.
  - **osm.gpkg**: contains base map data taken from [Open Street Maps](https://www.openstreetmap.org/)
  - **base data**: 
    - **contour.gpkg**: topography contour lines, that join points of equal elevation above a given level, such as mean sea level
    - **dtm.tif**: raster data of Data Terrain Model
    - **hillshade.tif**: hillshade raster data used to represent the morphology of terrain. 

## Open the project

From project menu ->  Open (Ctrl - O) -> choose the folder where project was previously copied and choose the ethiopia.qgz file. 

![folder organization of WashUp project](WASH-UP_Project-QGIS_user_guide.assets/image-20230525205048495.png){width=180px}

\newpage

# WASH-UP User interface

All interactions with the WASH-UP project are developed using the QGIS GUI. Refer to the user [interface manual](https://docs.qgis.org/3.22/en/docs/user_manual/introduction/qgis_gui.html) for further information.

QGIS is and advanced GIS, it have a user friendly interface but with a lot of functionality. We start to know the main parts of the interface

![Main windows of WASH-UP QGIS project](WASH-UP_Project-QGIS_user_guide.assets/image-20230524211131035.png)

The elements in the figure above are:

1. Layers List / Browser Panel
2. Toolbars
3. Map canvas
4. Status bar
5. Side Toolbar
6. Locator bar

##  Layers List

In the Layers list, you can see a list, at any time, **of all the layers** available to you.

**Expanding collapsed items** (by clicking the arrow or plus symbol beside them) will provide you with more information on the layers current appearance.

**Hovering over the layer** will give you some basic information: layer name, type of geometry, coordinate reference system and the complete path of the location on your device.

**Right-clicking on a layer** will give you a menu with lots of extra options. You will be using some of them before long, so take a look around!

<img src="WASH-UP_Project-QGIS_user_guide.assets/image-20220707121528017.png" alt="Table of contents (TOC) with layer of the project" style="zoom: 80%;" />



**Note:** A vector layer is a dataset, usually of a specific kind of object, such as roads, trees, etc. A vector layer can consist of either points, lines or polygons.


##  Browser Panel

The QGIS Browser is a panel in QGIS that lets you **easily navigate in your database**. You can have access to common vector files (e.g. ESRI Shapefile or MapInfo files), databases (e.g. PostGIS, Oracle, SpatiaLite, GeoPackage or MSSQL Spatial) and WMS/WFS connections. You can also view your GRASS data.

If you have saved a project, the Browser Panel will also give you quick access to all the layers stored in the same path of the project file under in the [![qgsProjectFile](WASH-UP_Project-QGIS_user_guide.assets/mIconQgsProjectFile.png)] Project Home item.

Moreover, you can set one or more folder as **Favorites**: search under your path and once you have found the folder, right click on it and click on `Add as a Favorite`. You should then be able to see your folder in the [![favourites](WASH-UP_Project-QGIS_user_guide.assets/mIconFavourites.png)] Favorites item.

**Tip:**  It can happen that the folders added to Favorite item have a really long name: dont worry right-click on the path and choose `Rename Favorite...` to set another name.


## Toolbars

Your most often used sets of tools can be turned into toolbars for basic access. For example, the File toolbar allows you to save, load, print, and start a new project. You can easily customize the interface to see only the tools you use most often, adding or removing toolbars as necessary via the View  Toolbars menu.

Even if they are not visible in a toolbar, all of your tools will remain accessible via the menus. For example, if you remove the File toolbar (which contains the Save button), you can still save your map by clicking on the Project menu and then clicking on Save.

##  Map Canvas

This is where the map itself is displayed and where layers are loaded. In the map canvas you can interact with the visible layers: zoom in/out, move the map, select features and many other operations that we will deeply see in the next sections.

![Map Canvas](WASH-UP_Project-QGIS_user_guide.assets/image-20220707121636915.png)

## Status Bar

Shows you information about the current map. Also allows you to adjust the map scale, the map rotation and see the mouse cursors coordinates on the map.

![Status bar](WASH-UP_Project-QGIS_user_guide.assets/image-20230525210352385.png)

## Side Toolbar

By default the Side toolbar contains the buttons to load the layer and all the buttons to create a new layer. But remember that you can move all the toolbars wherever it is more comfortable for you.

## Locator Bar

Within this bar you can access to almost all the objects of QGIS: layers, layer features, algorithms, spatial bookmarks, etc. Check all the different options in the [Locator Settings](https://docs.qgis.org/3.22/en/docs/user_manual/introduction/qgis_configuration.html#locator-options) section of the QGIS User Manual.

**Tip:** With the shortcut Ctrl+K you can easily access the bar.

\newpage

# Navigating the Map Canvas

There are multiple ways to zoom and pan to an area of interest. You can use the Map Navigation toolbar, the mouse and keyboard on the map canvas and also the menu actions from the View menu and the layers contextual menu in the Layers panel.



| Icon                                                         | Label                     | Usage                                                        |
| ------------------------------------------------------------ | ------------------------- | ------------------------------------------------------------ |
| [![pan](WASH-UP_Project-QGIS_user_guide.assets/mActionPan.png)] | Pan Map                   | When activated, left click anywhere on the map canvas to pan the map at the cursor position. You can also pan the map by holding down the left mouse button and dragging the map canvas. |
| [![zoomIn](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomIn.png)] | Zoom In                   | When activated, left click anywhere on the map canvas to zoom in one level. The mouse cursor position will be the center of the zoomed area of interest. You can also zoom in to an area by dragging a rectangle on the map canvas with the left mouse button. |
| [![zoomOut](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomOut.png)] | Zoom Out                  | When activated, left click anywhere on the map canvas to zoom out one level. The mouse cursor position will be the center of the zoomed area of interest. You can also zoom out from an area by dragging a rectangle on the map canvas with the left mouse button. |
| [![panToSelected](WASH-UP_Project-QGIS_user_guide.assets/mActionPanToSelected.png)] | Pan Map to Selection      | Pan the map to the selected features of all the selected layers in the Layers panel. |
| [![zoomToSelected](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomToSelected.png)] | Zoom To Selection         | Zoom to the selected features of all the selected layers in the Layers panel. |
| [![zoomToLayer](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomToLayer.png)] | Zoom To Layer(s)          | Zoom to the extent of all the selected layers in the Layers panel. |
| [![zoomFullExtent](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomFullExtent.png)] | Zoom Full                 | Zoom to the extent of all the layers in the project or to the [project full extent] |
| [![zoomLast](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomLast.png)] | Zoom Last                 | Zoom the map to the previous extent in history.              |
| [![zoomNext](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomNext.png)] | Zoom Next                 | Zoom the map to the next extent in history.                  |
| [![zoomActual](WASH-UP_Project-QGIS_user_guide.assets/mActionZoomActual.png)] | Zoom to Native Resolution | Zoom the map to a level where one pixel of the active raster layer covers one screen pixel. |

Table: Main navigation commands

It's also possible to interact with the data through this tools:

| [![openTable](WASH-UP_Project-QGIS_user_guide.assets/mActionOpenTable.png)] **Open Attribute Table** | [![identify](WASH-UP_Project-QGIS_user_guide.assets/mActionIdentify.png)] **Identify Features** |
| ------------------------------------------------------------ | ------------------------------------------------------------ |

Table: Alphanumeric querying tools

The "Identify Feature" tool open the "info windows" that show information about the WSI. Press the button in left top of the windows to open the related forms developed for the specific layers of the WASH-UP project to edit data.

![The identify panel show main information about WSI. Whit the top-left button you can open the form view](WASH-UP_Project-QGIS_user_guide.assets/image-20230525210603253.png){width=150px}

\newpage

# Edit data with the WashUP QGIS Project 

## Layers

The WashUp project contains different layers organized by groups. Every group represents a class of objects 

Those are the main classes:

- **Water Supply Infrastructures:** contains 9 different types of infrastructures with different characteristic. All classes are point except conducts that have lines geometry. There's also a special class called **Hydroscheme** that doesn't have a geometry type, used to link together different classes in a single Hydrological structure.
- **Water Supply Resources:** contains potential or not exploited water sources  
- **OSM:** this group collect a lot of vector data concerning information about topography, roads, buildings, etc. Alla data are stored locally and not need an internet connection. 
- **BASE LAYER**: Contains boundary limits of regions, woredas, kebele and villages.
- **BASE TOPOGRAPY:** Contains WMS link that allow to use some base map like OSM, Google satellite or OpenTopoMap. A working internet connection is mandatory ! 

  

## Insert new data

In order to insert a new object into a classes you need to enter **edit mode**. GIS software commonly requires this to prevent you from accidentally editing or deleting important data. Edit mode is switched on or off individually for each layer 

In order to you had to:

- choose the WSI layer with type of WSI you want to edit in the layer list
- click on toogle editing icon ![image-20220715060714543](WASH-UP_Project-QGIS_user_guide.assets/image-20220715060714543.png){ width=16px }
- insert point on the map.

Several tools will appear on the toolbar: 

- point features ![capturePoint](WASH-UP_Project-QGIS_user_guide.assets/mActionCapturePoint.png){ width=16px }Add Point Feature

- linear features ![captureLine](WASH-UP_Project-QGIS_user_guide.assets/mActionCaptureLine.png){ width=16px } Add Line button

- polygonal features ![capturePolygon](WASH-UP_Project-QGIS_user_guide.assets/mActionCapturePolygon.png){ width=16px } Capture Polygon
  
In this project we don't use polygonal features.

After we had digitized the geometry a mask is opened and we can insert alphanumeric data. The mask contain 5 or 6 tabs (see **"Interact with forms"** chapter for details).

At the end of digitizing click again on ![toggleEditing](WASH-UP_Project-QGIS_user_guide.assets/mActionToggleEditing.png){ width=16px } Toggle Editing button and save your work.
  
  
**Remeber:** The WASH-UP QGIS project allows to edit also non geographic layer. For example the **hydroscheme table** can be edited using ![newTableRow](WASH-UP_Project-QGIS_user_guide.assets/mActionNewTableRow.png){ width=16px } Add Record button.

![Example of hydroscheme form, a layer without geometry](WASH-UP_Project-QGIS_user_guide.assets/hydroscheme.png)



## Edit data step by step

1. choose the layer to edit: select it on the TOC, it will be colored in blue

2. Click on the ![toggleEditing](WASH-UP_Project-QGIS_user_guide.assets/mActionToggleEditing.png){ width=16px } Toggle Editing button to start editing

3. add point or line features using ![capturePoint](WASH-UP_Project-QGIS_user_guide.assets/mActionCapturePoint.png){ width=16px } Add Point Feature or  ![captureLine](WASH-UP_Project-QGIS_user_guide.assets/mActionCaptureLine.png){ width=16px } Add Line button

4. edit data using the form

**Attention:**
Edit the data for general descriptive data and specific object data, then click the OK button at the bottom right of the form. Then save the data using the <img src="WASH-UP_Project-QGIS_user_guide.assets/mActionSaveEdits.png" alt="saveEdits" style="zoom:75%;" /> Save Layer Edits button. **It is mandatory to save the data before changing the data for the next folder.**


5. continue editing a new geometric feature or continue the descriptions for the functionality, quality and documentation folders



## Interact with forms

The modules are grouped in folders to show the data in a simple and clear way.
In the QGIS WASH-UP project the folder contains: general descriptive data, specific object data, functionality information, water quality data and other additional descriptions such as photos or documents.

### General descriptive data

The first tab contains general data (localization as Region, Woreda, Kebele, Village,etc).  The coordinate of the point in the selected system (EPSG:3857 by default) and the elevation. Some data is automatically compiled by the system, some other data need the action of the user to input them. This tab is the same for all classes.

![General information of WSI](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_1j.png)

### Specific object data
The second, and in some cased the third tabs, contain the specific technical data of the WSI. 

![Technical data of WSI](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_2j.png){width=450px}

In some cases, when we have a lot of files, technical data can be divided in two or more tabs (depending on chosen Water Supply infrastructure).

![Other technical data](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_3j.png){width=450px}

### Functionality information

In this tab you can add the status of functionality of a specific water supply system. The functionality is always referred to a specific date. You can add more than one status, in order to maintain history ho the problem and the technical intervention, so it's possible to monitoring the state of the WSI during its life. 

<div style="text-align:center; font-size:30px; color:purple"><b>Before insert data in this section is mandatory to save new object.</div>

![Functionality information](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_41j.png){width=450px}

- press ok and close form 
- save layer ![image-20220715210549699](WASH-UP_Project-QGIS_user_guide.assets/image-20220715210549699.png){width=16px} 

- reopen form with identify tool ![image-20220715210632863](WASH-UP_Project-QGIS_user_guide.assets/image-20220715210632863.png){width=16px}  and with ![image-20220715210723877](WASH-UP_Project-QGIS_user_guide.assets/image-20220715210723877.png){ width=150px } 

- insert new row with the yellow pencil  

![press the yellow pencil icon to switch to editing mode](WASH-UP_Project-QGIS_user_guide.assets/image-20230525154543693.png){ width=150px }

Now you can insert functionality information.

![Functionality form](WASH-UP_Project-QGIS_user_guide.assets/image-20230525154704998.png){width=450px}

It's possible to report this main data to describe the functionality:

**Operating state:** 

- Functioning: at the time of the inspection the WSI is in good condition and fully functional
- Not functioning: at the time of the inspection the WSI is damaged and not working
- Partially functioning: at the time of the inspection the WSI reports some problems that may limit its functionality

**Reporting date:** the date the report refers to. It's possible to insert how many dates you want in order to maintain the whole history of the state and of the technical maintenance

**Intervention type**: 

- ordinary maintenance
- extraordinary maintenance
- survey

**Kind of problem:** used to describe the typology of detected problem

- No problem: the WSI is fine

- Damaged: some problem reported 

- Demolished: the WSI is totally damaged and impossible (or very hard) to repair 

- Not enough water: 

- Water quality problem: 

- Finance and management problem

- Other

**Description:** it's possible to write some notes of the state of functionality or insert a detailed report of problem detected.

![List of all monitoring reports](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_42j.png){width=450px}

When you finished to insert data press OK an then save functionality information with the blue disk. 

  ![Save intervention data](WASH-UP_Project-QGIS_user_guide.assets/image-20230525154825105.png){ width=150px }



### Water quality data

This section collect information about the water quality. Every record of this section is referred to a specific date, so it's possible to monitoring the change of the water quality of the WSI. 

![Water quality form](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_51j.png){width=450px}

**Parameter:** first of all we have to choose the measured parameter.  It's possible to choose from a list of 41 chemical of physical parameter. 

| **parameter**                | **unit**        |
| ---------------------------- | --------------- |
| Coliform                     | CFU/ml          |
| E. Coli                      | CFU/ml          |
| Faecal Streptococci          | CFU/ml          |
| Staphylococcus aureus        | CFU/ml          |
| Vibrio                       | CFU/ml          |
| Listeria                     | CFU/ml          |
| Clostridia                   | CFU/ml          |
| Pseudomonas                  | CFU/ml          |
| Yeast/Moulds                 | CFU/ml          |
| Aerobic Mesophilic count     | CFU/ml          |
| Turbidity                    | NTU             |
| TSS (Total suspended solids) | ppm             |
| Conductivity                 | (mic.simens/cm) |
| PH                           | ppm             |
| BOD                          | ppm             |
| COD                          | ppm             |
| TOC                          | ppm             |
| Shigella                     | In 25ml         |
| Salmonella                   | In 25ml         |
| H2S                          | ppm             |
| Sr                           | ppm             |
| Ba                           | ppm             |
| NH4                          | ppm             |
| K                            | ppm             |
| Na                           | ppm             |
| Mg                           | ppm             |
| Ca                           | ppm             |
| SO4                          | ppm             |
| Cl                           | ppm             |
| HCO3                         | ppm             |
| CO3                          | ppm             |
| Mn                           | ppm             |
| Pb                           | ppm             |
| Cu                           | ppm             |
| Al                           | ppm             |
| Fe                           | ppm             |
| SiO2                         | ppm             |
| B                            | ppm             |
| PO4                          | ppm             |
| NO3                          | ppm             |
| F                            | ppm             |

Table: List of available parameters

**Sampling date:** date of sampling for the specific parameter

**Sampling date:** analysis date 

**Value:** report the value of measured parameter

It's possible to report the value of different parameters or the same parameter measured in different dates. Yuo have to insert one for every value.

![List of collected water quality monitoring data](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_52j.png){width=450px}

### Additional descriptions

In this section it's possible to insert the photographic documentation. The photo must be first copied in the local disk and then linked using the "disk path or url". It's also possible to link an internet image using the URL.

We can also insert some other information like: a short name, the photo shooting date and a detailed description. 

![Photographic documentation](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_61j.png){width=450px}

![View with all documentation](WASH-UP_Project-QGIS_user_guide.assets/wsi_borehole_woe_62j.png){width=450px}

\newpage
## Hydroscheme

Water supply infrastructures that are physically linked together can be grouped in an hydroscheme. 

![example of hydroscheme](WASH-UP_Project-QGIS_user_guide.assets/image-20220715212342014.png)

Before assign a WSI to an hydroscheme it must be created!

To insert a new hydroscheme:

- select hydroscheme layer  ![image](WASH-UP_Project-QGIS_user_guide.assets/image-20220715212602881.png){width=128px}
- press edit button   ![image](WASH-UP_Project-QGIS_user_guide.assets/image-20220715210549699-1685022608089-4.png){width=16px}
- insert a new record ![image-20220715212853632](WASH-UP_Project-QGIS_user_guide.assets/image-20220715212853632.png){width=16px}
- compile form with hydroscheme name
- save with blue disk ![image-20220715212826486](WASH-UP_Project-QGIS_user_guide.assets/image-20220715212826486.png){width=16px}

Now when you insert or edit a WSI you can assign a WSI to an Hydroscheme choosing his name.

![select Hydroscheme name in WSI form to link the object togheter](WASH-UP_Project-QGIS_user_guide.assets/image-20220715213133378.png){width=300px} 


\newpage
## Edit conducts with topology 

If WSI are physical connected by pipelines (conducts) it's possible to draw it as lines features. 

- To insert a new pipeline select conduct layer in the layer list ![](WASH-UP_Project-QGIS_user_guide.assets/image-20220715213524900.png){width=100px}  and press start editing ![](WASH-UP_Project-QGIS_user_guide.assets/image-20220715213613294.png){width=16px}

- Now to physically connect conducts with other WSI you have to activate snap ![](WASH-UP_Project-QGIS_user_guide.assets/image-20220715213729115.png){width=180px}

- Remeber also to choose snap to all layers ![](WASH-UP_Project-QGIS_user_guide.assets/image-20220715213815331.png){width=16px}

Now you can digitize your pipeline.



# Insert point at specific coordinates

If you want insert a point at specific coordinates it's possible to use the coordinator plugin:

- If not installed, pleas install the plugin (See thne chapter install plugin)

- Open coordinator from Plugin -> Open coordinator Menu

- Insert coordinate and click on pan icon

![Coordinator Panel](WASH-UP_Project-QGIS_user_guide.assets/image-20230617050356789.png){width=250px}

- Insert point on to the red dot with usual procedure (see "Insert new data" paragraph)

![Map with the selected coordinates](WASH-UP_Project-QGIS_user_guide.assets/image-20230617050541152.png)


# Install a plugin

in order to install new plugin:

- go to Plugin -> Manage and Install plugin
- Select "install from zip"
- Select the plugin from folder plugin in your USB stick
- Close the windows

# Find data

## Locator bar

The Locator bar is a fast, always ready, generic and pluggable search facility in the Left Bottom corner of QGIS. Currently it is able to search layers but also a lot of other QGIS features like algorithms or actions. It's also possible to select a feature in current active layer by typing an attribute value.

- Press CTRL+K or click in the bottom left corner of QGIS windows and type the first part of layer name you are searching for
- click on layer you want to select

![In the left-bottom part of the window yuou can find the location bar (see red square)](WASH-UP_Project-QGIS_user_guide.assets/image-20230525135735890.png){with=350px}

It' also possible search specific record in a table using the find function. 

- Select the layer where you want search;
- press F3, the mask will open;
- you can select a value and a search condition. If more than one field are selected all conditions must be met (AND condition). In the example I select all hand dug well that is more than 5 meters depth. 

  

![Search Mask (press F3)](WASH-UP_Project-QGIS_user_guide.assets/image-20230525140156160.png)


\newpage
# Base layer

The WashUp contains some preloaded base data (see Getting started - base data). 

Information is organized in 3 main group:

**OSM**: Data derived from [OpenStreetMap](www.openstreetmap.org). Here you can find geographic information about streets, building, landuse, rivers etc... Alla data are stored locally and not need an Internet connection. 

**BASE LAYER**: Boundary level of Regions, Woreda, Kebele and Willage.

**BASE TOPOGRAPY:** it contains WMS link that allow to use some base map like OSM, Google satellite or OpenTopoMap. **A working Internet connection is mandatory !** 


![QGIS TOC with organization of layer in groups](WASH-UP_Project-QGIS_user_guide.assets/image-20230525141023684.png){width=200px}

# Additional functions

The software allow to use some additional functions using the **processing toolbox**. Some new tools were created in order to perform specific operations:

- exporting data to excel
- Create a functionality map
- Export quality
- Export Hydroscheme

To use the additional tool: 

- open Processing toolbox from Processing menu -> Toolbox or with processing icon ![](WASH-UP_Project-QGIS_user_guide.assets/image-20230617044654599.png) {width=16px}

- expand Project models

- choose the model you want to use

## Exporting data in Excel

With geoprocessing tools is possible to export data into excel format. 

- open Processing toolbox from Processing menu -> Toolbox or with processing icon ![](WASH-UP_Project-QGIS_user_guide.assets/image-20230617044654599.png){width=16px} 

- expand Project models -> washUP_export

- choose a specific WSI you want to export or export_total_inventory to export all information  

![WashUP processing toolbox to export data in excel](WASH-UP_Project-QGIS_user_guide.assets/image-20230617044801412.png){width=250px}

- Select e filename from button and choose e filename

  ![Choose filename where export data](WASH-UP_Project-QGIS_user_guide.assets/image-20230617045049681.png){width=450px}


It create an excel file containing the all data about the WSI: main information, functionality, quality information and photo.


![Main page of the excel file with information about WSI](WASH-UP_Project-QGIS_user_guide.assets/image-20231214225912130.png){width=450px} 



![Second page of the excel file with information about functionality](WASH-UP_Project-QGIS_user_guide.assets/image-20231214225959964.png){width=450px}

![Third page of the excel file with information about photo](WASH-UP_Project-QGIS_user_guide.assets/image-20231214230039804.png){width=450px}

![Fourth page of the excel file with information about quality](WASH-UP_Project-QGIS_user_guide.assets/image-20231214230105490.png){width=450px}


## Create a functionality map

With geoprocessing tools is possible to create a new layer with information about the last functionality states.

- open geoprocessing toolbox from Processing menu -> Toolbox or with processing icon ![](WASH-UP_Project-QGIS_user_guide.assets/image-20230617044654599.png){width=16px} 

- expand Project models -> washUP_functionality

- choose a specific WSI you want to create functionality map  

![WashUP processing toolbox to create functionality map](WASH-UP_Project-QGIS_user_guide.assets/image-20230617045338211.png){width=250px}

Processing will create a new layer that contains information about the current functionality of the WSI.

![Functionality map](WASH-UP_Project-QGIS_user_guide.assets/image-20230617045457432.png){width=450px}

![Functionality class legend](WASH-UP_Project-QGIS_user_guide.assets/image-20230617045539804.png){width=250px}

![Example of functionality information that report some problem](WASH-UP_Project-QGIS_user_guide.assets/image-20231214215209424.png){width=450px}

![Example of functionality information after a tecnical intervention](WASH-UP_Project-QGIS_user_guide.assets/image-20231214215228314.png){width=450px}

The processing take the last information and associate it with the geometry, allow creating a map of the functionality. 
  
\newpage
## Export quality

With geoprocessing tools is possible to export quality data to excel file with all connected information in order to create graphs:

- open geoprocessing toolbox from Processing menu -> Toolbox or with processing icon ![](WASH-UP_Project-QGIS_user_guide.assets/image-20230617044654599.png){width=16px} 
- expand Project models -> washUP_functionality
- choose a specific WSI you want to export information or choose export the total inventory  
  
    ![WashUP processing toolbox to export total quality](WASH-UP_Project-QGIS_user_guide.assets/image-20230617045827588.png){width=250px}
  
    ![Choose filename where export data](WASH-UP_Project-QGIS_user_guide.assets/image-20230617045924306.png){width=450px}



The processing export an excel file containing all information about quality quality data.

## Export Hydroscheme

With the hydroscheme processing fucntion is possible to aggregate all WSI that belong to a specific hydroscheme. Processing create a new layer containing the poligonal envelope of hydroscheme WSI and a summary of the number of  WSI grouped by category.

![Processing menu with hydroscheme function](WASH-UP_Project-QGIS_user_guide.assets/image-20231216104227398.png){width=250px}

![Output of Hydroscheme processing](WASH-UP_Project-QGIS_user_guide.assets/image-20231216103456458.png){width=450px}

![Associated data to Hydroscheme processing - Table view](WASH-UP_Project-QGIS_user_guide.assets/image-20231216103646420.png){width=450px}

![Associated data to Hydroscheme processing - Form view](WASH-UP_Project-QGIS_user_guide.assets/image-20231216103721991.png){width=450px}

\newpage
## Create the global inventory 

With total inventory tool you can create a new layer containing all WSI collected in your project. Of course, the new table layer contains only common information about WSI. For example the administrative information are common for all WSI and are reported into the table, but an information as the "borehole depth" is specific just fore some WSI, so it's not reported into the global inventory table.

![WSI total inventory tool](WASH-UP_Project-QGIS_user_guide.assets/image-20231216104343857.png){width=250px}

![Output of WSI total inventory tool](WASH-UP_Project-QGIS_user_guide.assets/image-20231216104833641.png){width=450px}

![Data associated with WSI total inventory tool output - form view](WASH-UP_Project-QGIS_user_guide.assets/image-20231216104714708.png){width=450px}


\newpage
# Backup your data

All data collected into the software are stored in one specific file, named **ha_data.gpkg**. the file is contained in the main folder. To preserve your data from accidental lost is a good idea create a copy of this file in a usb stick or in any other external storage device. 

It's also possible to share data with colleagues or other people simply sharing this file and replacing it in the main folder. If you have some data remember to create a copy of the file before replacing it with another file.    

# Reference

QGIS is a complex software with a lot of features. This manual is not exhaustive and cover only the specific functionality of WashUp project. For any other information about  QGIS function refer to official [QGIS user Manual](https://docs.qgis.org/3.28/en/docs/user_manual/index.html).
